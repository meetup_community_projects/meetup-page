# Meetup Web Page

This project creates serverless web page for generic sites.

## Installation instructions

Prerequisites
- AWS CLI installed with correct credentials which has access to 
    - S3
    - Route 53
    - Dynamo DB
    - Cognito
    - Lambda
    - API gateway

- run the command which will ask for the following info
    - Web page name
    - Domain name
    - (optional) notification email address

- It will return the following info
    - S3 bucket where the email is served
    - DynamoDB Table name

## Requirements and Design

- Application should be serverless
    - Application should create an S3 bucket for serving web page
    - Application should create Lamda functions and SAM templates
- Application should support mobile
- Application should set the A records at Amazon Route 53
- Application should provide DynamoDb table to store blog metadata
    - blogs should be available on the site automatically after updaled to S3
- Application should have an initial home page 
- Application should allow sending messages to meetup domain "support@programming-tools-meetup.cloud"
- Application should provide login to see blogs 
    - User can login using facebook, google, or simple auth.
- Application could provide a Web User interface to show some statistics
    - Number of visitors
    - Map view of the visitor's ip addresses
- Application could provide streaming content

## Architecture  

![here](docs/architecture.jpg)

## Legal

This repository is maintained by Melbourne "AWS Programming and Tools Meetup" at https://www.meetup.com/Melbourne-AWS-Programming-and-Tools-Meetup/. This repository is protected under MIT Licence which can be found at https://opensource.org/licenses/MIT and repeated below

Copyright (c) <2018> <AWS Programming and Tools Meetup>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

